ROUGH DRAFT NOT FOR PUBLIC DISTRIBUTION!
----------------------------------------

# Quadratic Ascension 
and the cautionary tale of how
### Jank destroyed my ~~dreams~~ memes 

Hello!

I'm Ryan, coordinator for GitcoinDAO's Moonshot Collective, our rapid prototyping workstream for building coordination tools. 

We know it's all coordination - always has been!

{always has been meme here}

Consider the above [meme](https://en.wikipedia.org/wiki/Meme) - it has been used to positive effect on coordination inside Moonshot Collective and GitcoinDAO as well as across the Web3 ecosystem. 
Without retreading the entire history of the philosophy of ideas (don't tempt me!), let's agree that simpler memes are easier to spread and that their impact depends on distribution.

Well-composed simple ideas can be extremely effective coordination tools, powerful enough to rally a revolution - as we've seen in the powerful slogans and banners of revolutions past. 

As a slogan "It's all coordination - always has been" is pretty good, but it's a bit long and the context is implicit, so it only works for believers. Something shorter and more directive like "SLAY MOLOCH!" might be more powerful, but it only works for those who know Moloch is the demon-god of coordination failure. I don't have all the answers yet, but we're working on it! {link to Meditations on Moloch and maybe a shorter primer}

In mathematics, ideas that are simple to the point of beauty are called 'elegant', and when we're building digital tooling (programming is applied mathematics) the concept of elegance is the standard by which we judge our work. 

In memetics, I'm not sure we have a direct equivalent, so I'm going to speak of elegant memes and hope you understand that I mean memes that are simple enough to be beautiful and powerful. Memes with mimetic power (mimetics is a whole other can of worms, more on that later)

In practice, simple takes time because the world is complex and complicated. Patient, deliberate work is required to digest complexity and arrive at the sophistication of simplicity. 

One of my great joys in life, and one of the highest-leverage long-range investments I'm making in my work, is my work with the glorious memelords in MMM, our Memes Marketing & Merch workstream. I have been working with them for weeks on the difficult work of chewing down ideas into useful memes to coordinate our community.  Our work is in its earliest stages now, but already we're seeing strange and wonderful fruit - and Owocki likes 'em so we can't be that far off :) {link to twitter, embed screenshots linked, don't embed twitter itself, that's always garbage}

Then tragedy struck, setting back the future. {link to Erdos story about god's book, elegance, and the month that mathematcis lost}
All my work, all my ideas, all my proposals and plans and musings and dreams, everything I had contributed to MMM was obliterated in an instant, for reasons that are amusing and ironic but not actually important. (tell that story in a footnote, maybe also include all your original flowery prose)

The underlying reason this happened is because we trusted a proprietary closed platform with our precious work. 

{meme: fell victim to one of the classic blunders!}

We don't need to be shy about our love for public goods, especially 'Free as in Freedom' Free Software at the core of our mission. If it's core to our mission, why isn't it core to our operations?

It's not cowardice or hypocrisy - it's a simple accident of history. Gitcoin began as a company, and the process of ascending to DAOhood is not simple or easy, and prioritizing the decentralization of workstreams and governance has been wise. It's important to have a sense of priorities.

Moonshot Collective is building coordination tools and prototyping the public goods future, and our members are working in groups within Gitcoin to complete the GitcoinDAO ascension to take us to the promised quadratic lands. Our priorities flow from our principles, and drive our sincere ambition to coordinate the world to slay moloch and join/found the federation.

Moonshot Collective is officially commencing Operation Quadratic Ascension, and our internal working group will be planning to transition all our operations to Free as in Freedom technologies. We're committed to not only developing Free and Open Source software tools {fsf video w mo? 35 anniversary video}, but to making Moonshot Collective and our subgroups (and eventually GitcoinDAO) forkable organizations.

In the long term, this will mean MC will be Web3 native / Web3 first, self-sovereign / self-hosting , (distributed? decentralized?) (probably pick 3/4 official guiding principles to rally OQA around), and we'll have starter kits and playbooks to help others, and working groups devoted to midwifing the ascension of all our eager ecosystem partners.

In the medium term, this means getting MC getting to a point of being self-hosting, with no jank anywhere in our core stack, including comms.

In the short term, this means we're going to eat a lot of dogfood. {link explaining dogfooding}
Our first feast will begin at our official canonical channel on our Mattermost (willing/eager to consider alternatives) and first we'll be transitioning MC members off of Telegram and Discord.
We know big change takes time, so we'll be taking a careful approach, building bridges between the old world and the next, and holding the hands of everyone as we cross the bridge to the promised quadratic lands. 
This means I'll be working with MMM using a mattermost-discord bridge, (until MMM ascends (which may not be for many months)).

Are you ready to join us in the promised land?
"When we hold our own keys, None can expel us from paradise."
Join up and carry out your mission!

{I want you / I'm doing my part memes? too militaristic?}
